package com.rastrahm.app.listtask;

public interface DialogEventHandler {
    void getAction();
    void searchTasks(String[] search);
}
