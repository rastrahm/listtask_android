package com.rastrahm.app.listtask.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

import androidx.fragment.app.DialogFragment;

import com.rastrahm.app.listtask.R;
import com.rastrahm.app.listtask.controler.Config;
import com.rastrahm.app.listtask.controler.Util;

public class Configuration extends DialogFragment {

    private String Title;

    EditText EditFile;
    EditText EditUrl;
    EditText EditUser;
    EditText EditPass;
    RadioButton RadioFile;
    RadioButton RadioUrl;


    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    @Override
    public Dialog onCreateDialog(Bundle data) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_config, null);
        EditText EditFile = dialogView.findViewById(R.id.editTextFileName);
        EditText EditUrl = dialogView.findViewById(R.id.editTextServer);
        EditText EditUser = dialogView.findViewById(R.id.editTextUser);
        EditText EditPass = dialogView.findViewById(R.id.editTextPassword);
        RadioButton RadioFile = dialogView.findViewById(R.id.radioFile);
        RadioButton RadioUrl = dialogView.findViewById(R.id.radioUrl);

        builder.setView(dialogView);
        builder.setTitle(Title);
        Config conf = new Config("res/raw/listtask.properties");
        EditFile.setText(conf.getProperty("file"));
        EditUrl.setText(conf.getProperty("url"));
        EditUser.setText(conf.getProperty("user"));
        EditPass.setText(conf.getProperty("pass"));
        if (conf.getProperty("method").equals("file")) {
            RadioFile.setChecked(true);
            RadioUrl.setChecked(false);
        } else {
            RadioFile.setChecked(false);
            RadioUrl.setChecked(true);
        }
        builder.setPositiveButton(R.string.dialog_edit_new_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Util util = new Util();
                conf.setProperty("file", EditFile.getText().toString());
                conf.setProperty("url", EditUrl.getText().toString());
                conf.setProperty("user", EditUser.getText().toString());
                conf.setProperty("pass", EditPass.getText().toString());
                if (RadioFile.isChecked()) {
                    conf.setProperty("method", "file");
                } else {
                    conf.setProperty("method", "url");
                }
                conf.saveProperty("res/raw/listtask.properties");
            }
        });
        builder.setNegativeButton(R.string.dialog_edit_new_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        return builder.create();
    }
}
