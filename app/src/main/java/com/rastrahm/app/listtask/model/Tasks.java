package com.rastrahm.app.listtask.model;

/**
 * Clase base del sistema, que involucra a cada una de las tareas
 * Su estructura comprende un Id y el Cont que corresponde al contenido
 * 
 * @author Rolando Strahm
 *
 */

public class Tasks {

    private int id;

    private String name;

    /**
     * Constructor en blanco
     */
    public Tasks() {
        super();
    }

    /**
     * Contructor de un elemento con valores
     * @param id : int del id de la tarea
     * @param cont : String con el contenido de la tarea
     */
    public Tasks(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    /**
     * Retorna el Id de la tarea
     * @return int	: Id de la operación
     */
    public int getId() {
        return this.id;
}

    /**
     * Pone un Id de una tarea
     * @param id : int con el Id de la tarea
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retorna el contenido de la tarea
     * @return String	: Contiene el texto de la tarea
     */
    public String getName() {
        return this.name;
    }

    /**
     * Pone el string de la tarea
     * @param cont : String con el contenido de la tarea
     */
    public void setCont(String name) {
        this.name = name;
    }

    /**
     * Transforma los valores a una cadena de texto
     * @return String: Retorna una cadena con los valores que contiene la tarea
     */
    @Override
    public String toString() {
        return "Id= " + this.id + ", Value= " + this.name;
    }
}
