package com.rastrahm.app.listtask.controler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Utilidades de manejo de archivos
 * @author Rolando Stahm
 */
public class FileUtils {
    /**
     * Detemina que la ruta exista y sea un directorio, en caso de que no exista la crea y si detecta un archivo con ese nombre retorna falso
     * @param path : String Ruta del directorio
     * @return boolean : True: El directorio existe o fue creado, False: No se pudo crear el directorio 
     */
    public static boolean Dir(String path) {
        File target = new File(path);
        if (target.exists() && target.isDirectory()) {
            return true;
        } else if (target.exists() && target.isFile()) {
            return false;
        } else {
            return target.mkdir();
        }
    }
	
    /**
     * Escribe el contenido en un archivo, retorna true, en caso de error retorna falso
     * @param ruta : String Ruta del archivo
     * @param fileName : String Nombre del archivo
     * @param fileContent : String Contenido a guardar
     * @return boolena : True: Archivo guardado, False: Error en la escritura
     */
    public static boolean Writer(String ruta, String fileName, String fileContent) {
        try {
            FileWriter writer = new FileWriter(ruta + "/" + fileName);
            writer.write(fileContent);
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
	
    /**
     * fileToString Función que lee el contenido de un archivo y retorna un String
     * @param file:		String Ruta al archivo
     * @return String:	Contenido del archivo
     */
    public static String Read(String file) {
        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                contentBuilder.append(sCurrentLine);
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }
}
