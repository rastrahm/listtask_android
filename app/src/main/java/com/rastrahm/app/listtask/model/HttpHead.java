/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rastrahm.app.listtask.model;

import com.rastrahm.app.listtask.model.Data;

/**
 * Clase que contiene los valores globales de los datos recibidos y el token de la aplicación
 * @author Rolando Strahm
 */
public class HttpHead {
    Data[] data;
    String jwt;

    /**
     * Constructor en blanco
     */
    public HttpHead() {
    }

    /**
     * Contructor con datos
     * @param data : Array de Data
     * @param jwt : String con el token
     * @see com.rastrahm.app.listtask.model.Data
     */
    public HttpHead(Data[] data, String jwt) {
        this.data = data;
        this.jwt = jwt;
    }
    
    /**
     * Retorna el contenido de los valores dara
     * @return Data : Array de data
     * @see com.rastrahm.app.listtask.model.Data
     */
    public Data[] getData() {
        return this.data;
    }

    /**
     * Retorna el token del JWT
     * @return String : String que contiene el token
     */
    public String getJwt() {
        return this.jwt;
    }

    /**
     * Coloca los data correspondientes
     * @param data : array de Data
     */
    public void setData(Data[] data) {
        this.data = data;
    }

    /**
     * Coloca el String con ek token JWT
     * @param jwt : String con el token
     */
    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
    
}
