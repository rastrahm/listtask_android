package com.rastrahm.app.listtask.model;

import java.util.List;

/**
 * Super clase (Clase Global) que agrupa a todos los grupos 
 * @author Rolando Strahm
 */

public class Groups {
	
    private List <TaskGroup> group;

    /**
     * Constructor en blanco de la clase
     */
    public Groups() {
        super();
    }

    /**
     * Consrtructor de la clase con los grupos
     * @param groups List<TaskGroup> Lista que contiene todos los grupos
     * @see com.rastrahm.app.listtask.model.TaskGroup
     */
    public Groups(List <TaskGroup> groups) {
        super();
        group = groups;
    }

    /**
     * Retorna los grupos
     * @return List TaskGroup Retorna la lista con los grupos de tareas
     * @see com.rastrahm.app.listtask.model.TaskGroup
     */
    public List <TaskGroup> getGroup() {
        return group;
    }

    /**
     * Pone la lista con los grupos de tarea
     * @param groups List GroupTask Coloca los grupos de tareas
     * @see com.rastrahm.app.listtask.model.TaskGroup
     */
    public void setGroup(List <TaskGroup> groups) {
        group = groups;
    }

    /**
     * Devuelveuna cadena con todos los taskGroup incluidos
     * @return String : Retorna una cadena con los TaskGroups
     */
    @Override
    public String toString() {
        String strValues = null;
        for (TaskGroup task : this.group) {
            strValues += task.toString() + "; ";
        }
        return strValues;
    }
    
    /**
     * Permite agregar un grupo de tareas al grupo
     * @param groups : TaskGroup nuevo
     */
    public void addGroup(TaskGroup groups) {
        group.add(groups);
    }
    
    /**
     * Devuleve el tamaño del objeto
     * @return int : Tamaño del List
     */
    public int sizeGroup() {
        return group.size();
    }
    
    /**
     * Devuelve el número secuencial del último grupo
     * @return int : Número del id
     */
    public int lastGroup() {
        return group.size() - 1;
    }

    public void removeGroup(int pos) {
        int cant = this.group.get(pos).getTasks().size();
        if (cant == 0) {
            this.group.remove(pos);
        }
    }
    /**
     * Devuelve el número correspondiente a un grupo buscado
     * @param value : String con la cadena a buscar
     * @return int : número del grupo buscado
     */
    public int searchGroup(String value) {
        int i = 0;
        for (TaskGroup groups : group) {
            if (groups.getName().equals(value)) {
                break;
            }
            i++;
        }
        return i;
    }
}
