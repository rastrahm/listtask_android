package com.rastrahm.app.listtask.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

import com.rastrahm.app.listtask.DialogEventHandler;
import com.rastrahm.app.listtask.R;
import com.rastrahm.app.listtask.controler.Util;
import com.rastrahm.app.listtask.model.GlobalGroups;
import com.rastrahm.app.listtask.model.TaskGroup;
import com.rastrahm.app.listtask.model.Tasks;

import java.util.ArrayList;
import java.util.List;

public class EditNew extends DialogFragment {

    private String Title;
    private String Tasks;
    private String Group;
    private int Type;
    EditText EditGroup;
    EditText EditTasks;
    private DialogEventHandler listener;

    public String getTitle() {
        return Title;
    }

    public String getTasks() {
        return Tasks;
    }

    public String getGroup() {
        return Group;
    }

    public int getType() {
        return Type;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setTasks(String tasks) {
        Tasks = tasks;
    }

    public void setGroup(String group) {
        Group = group;
    }

    public void setType(int type) {
        Type = type;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            listener = (DialogEventHandler) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement DialogSinglePickerFragment");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle data) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_edit, null);
        EditText editGroup = dialogView.findViewById(R.id.group);
        EditText editTasks = dialogView.findViewById(R.id.tasks);
        builder.setView(dialogView);
        builder.setTitle(Title);
        editGroup.setText(Group);
        if (Type == 1) {
            editTasks.setText(Tasks);
        }
        builder.setPositiveButton(R.string.dialog_edit_new_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Util util = new Util();
                //Tipo 0, elemento nuevo
                if (Type == 0) {
                    //Nueva tarea
                    if (util.searchGroup(editGroup.getText().toString()) > -1) {
                        int i = util.searchGroup(editGroup.getText().toString());
                        com.rastrahm.app.listtask.model.Tasks task = new Tasks(GlobalGroups.getGlobal().getGroup().get(i).newId(), editTasks.getText().toString());
                        GlobalGroups.getGlobal().getGroup().get(i).addTask(task);
                    //Nueva tarea y grupo
                    } else {
                        com.rastrahm.app.listtask.model.Tasks task = new Tasks(0, editTasks.getText().toString());
                        List<Tasks> list = new ArrayList<Tasks>();
                        list.add(task);
                        TaskGroup group = new TaskGroup(GlobalGroups.getGlobal().getGroup().size(), editGroup.getText().toString(), list);
                        GlobalGroups.getGlobal().addGroup(group);
                    }
                // Tipo 1, editar elemento
                } else {
                    if (util.searchGroup(editGroup.getText().toString()) > -1) {
                        for (TaskGroup task : GlobalGroups.getGlobal().getGroup()) {
                            if (task.getName().equals(editGroup.getText().toString())) {
                                for (int i = 0; i < task.getTasks().size(); i++) {
                                    if (task.getTasks().get(i).getName().equals(Tasks)) {
                                        task.getTasks().get(i).setCont(editTasks.getText().toString());
                                        break;
                                    }
                                }
                            }
                        }
                    } else {
                        com.rastrahm.app.listtask.model.Tasks task = new Tasks(0, editGroup.getText().toString());
                        List<Tasks> list = new ArrayList<Tasks>();
                        list.add(task);
                        TaskGroup group = new TaskGroup(GlobalGroups.getGlobal().getGroup().size(), editGroup.getText().toString(), list);
                        GlobalGroups.getGlobal().addGroup(group);
                    }
                }
                listener.getAction();
            }
        });
        builder.setNeutralButton(R.string.dialog_edit_new_delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String group = editGroup.getText().toString();
                String value = editTasks.getText().toString();
                int pos = com.rastrahm.app.listtask.model.GlobalGroups.getGlobal().searchGroup(group);
                com.rastrahm.app.listtask.model.GlobalGroups.getGlobal().getGroup().get(pos).removeTask(value);
                if (com.rastrahm.app.listtask.model.GlobalGroups.getGlobal().getGroup().get(pos).getTasks().size() <= 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.dialog_edit_new_confirm_title);
                    builder.setMessage(R.string.dialog_edit_new_confirm_text);
                    builder.setPositiveButton(R.string.dialog_edit_new_confirm_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            com.rastrahm.app.listtask.model.GlobalGroups.setErraseGroup(true);
                            com.rastrahm.app.listtask.model.GlobalGroups.getGlobal().getGroup().remove(pos);
                            listener.getAction();
                        }
                    });
                    builder.setNegativeButton(R.string.dialog_edit_new_confirm_cancel,null);
                    builder.create();
                    builder.show();
                }
                listener.getAction();
            }
        });
        builder.setNegativeButton(R.string.dialog_edit_new_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    listener.getAction();
                    dialog.dismiss();
                }
            }
        });
        return builder.create();
    }
}
