/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rastrahm.app.listtask.model;

/**
 * Clase del objeto data del servicio web
 * @author Rolando Strahm
 */
public class Data {
    int big_id;
    int big_profile;

    /**
     * Contructor en blanco
     */
    public Data() {
    }

    /**
     * Contructor con datos
     * @param big_id : int Con el id del usuario
     * @param big_profile : int Con el perfil del usuario
     */
    public Data(int big_id, int big_profile) {
        this.big_id = big_id;
        this.big_profile = big_profile;
    }

    /**
     * Devuelve el id guardado
     * @return int : id guardado
     */
    public int getBig_id() {
        return big_id;
    }

    /**
     * Devuelve el número del perfil
     * @return int : número del perfil
     */
    public int getBig_profile() {
        return big_profile;
    }

    /**
     * Coloca el valor del Id
     * @param big_id : int Con Id
     */
    public void setBig_id(int big_id) {
        this.big_id = big_id;
    }

    /**
     * Coloca el valor del perfil
     * @param big_profile : int Con el número de perfil
     */
    public void setBig_profile(int big_profile) {
        this.big_profile = big_profile;
    }
    
}
