package com.rastrahm.app.listtask.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

import com.rastrahm.app.listtask.DialogEventHandler;
import com.rastrahm.app.listtask.R;
import com.rastrahm.app.listtask.controler.Util;

public class Search extends DialogFragment {

    EditText TextSearch;

    private DialogEventHandler listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            listener = (DialogEventHandler) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement DialogSinglePickerFragment");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle data) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_search, null);
        EditText TextSearch = dialogView.findViewById(R.id.search);
        builder.setView(dialogView);
        builder.setPositiveButton(R.string.dialog_search_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Util util = new Util();
                String search = TextSearch.getText().toString();
                String[] locate = util.searchString(search, com.rastrahm.app.listtask.model.GlobalGroups.getGlobal());
                if (locate[0] == null) {
                    //"No se encuentra en las tareas " + search
                } else {
                    listener.searchTasks(locate);
                }
            }
        });
        builder.setNegativeButton(R.string.dialog_edit_new_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        return builder.create();
    }
}
