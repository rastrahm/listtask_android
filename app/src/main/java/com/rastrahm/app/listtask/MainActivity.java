package com.rastrahm.app.listtask;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.rastrahm.app.listtask.controler.Util;
import com.rastrahm.app.listtask.model.GlobalGroups;
import com.rastrahm.app.listtask.model.Groups;
import com.rastrahm.app.listtask.view.Configuration;
import com.rastrahm.app.listtask.view.EditNew;
import com.rastrahm.app.listtask.view.Search;

import java.net.MalformedURLException;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, DialogEventHandler {

    ImageButton btnLoad;
    ImageButton btnEdit;
    ImageButton btnAdd;
    ImageButton btnConf;
    ImageButton btnFind;
    ImageButton btnExit;

    Spinner ComboBox;

    ListView List;

    String selTask;

    int posSelect;
    int selCombo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLoad = (ImageButton) findViewById(R.id.ButtonLoad);
        btnEdit = (ImageButton) findViewById(R.id.ButtonEdit);
        btnAdd  = (ImageButton) findViewById(R.id.ButtonAdd);
        btnConf = (ImageButton) findViewById(R.id.ButtonConfig);
        btnFind = (ImageButton) findViewById(R.id.ButtonSearch);
        btnExit = (ImageButton) findViewById(R.id.ButtonExit);

        btnLoad.setOnClickListener(this::onClick);
        btnEdit.setOnClickListener(this::onClick);
        btnAdd.setOnClickListener(this::onClick);
        btnConf.setOnClickListener(this::onClick);
        btnFind.setOnClickListener(this::onClick);
        btnExit.setOnClickListener(this::onClick);

        ComboBox = (Spinner) findViewById(R.id.spinner);
        ComboBox.setOnItemSelectedListener(this);

        List = (ListView) findViewById(R.id.list);
        List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selTask = parent.getItemAtPosition(position).toString();
                for (int i = 0; i < List.getChildCount(); i++) {
                    if(position == i ){
                        List.getChildAt(i).setBackgroundColor(Color.CYAN);
                    }else{
                        List.getChildAt(i).setBackgroundColor(Color.TRANSPARENT);
                    }
                }
            }
        });

        Context context = this;
        Util util = new Util();
        Groups groups = util.loadGroup();
        util.fillSpinner(ComboBox, groups);
        util.fillList(List, ComboBox, groups);
    }

    public void onClick(View v) {
        Util util = new Util();
        switch (v.getId()) {
            case R.id.ButtonLoad:
                util.fillList(List, ComboBox, GlobalGroups.getGlobal());
                break;
            case R.id.ButtonEdit:
                EditNew Edit = new EditNew();
                Edit.setTitle("Editar");
                Edit.setType(1);
                Edit.setGroup(ComboBox.getItemAtPosition(ComboBox.getSelectedItemPosition()).toString());
                Edit.setTasks(selTask);
                Edit.show(getSupportFragmentManager(), "EditNew");
                break;
            case R.id.ButtonAdd:
                EditNew Add = new EditNew();
                Add.setTitle("Nuevo");
                Add.setType(0);
                Add.setGroup(ComboBox.getItemAtPosition(ComboBox.getSelectedItemPosition()).toString());
                Add.show(getSupportFragmentManager(), "EditNew");
                break;
            case R.id.ButtonConfig:
                Configuration Conf = new Configuration();
                Conf.setTitle("Configuración");
                Conf.show(getSupportFragmentManager(), "Configuration");
                break;
            case R.id.ButtonSearch:
                Search search = new Search();
                search.show(getSupportFragmentManager(), "Search");
                break;
            case R.id.ButtonExit:
                try {
                    util.saveJson();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finish();
                System.exit(0);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + v.getId());
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selTask = parent.getItemAtPosition(position).toString();
        selCombo = position;
        TextView textView = (TextView) view;
        ((TextView) parent.getChildAt(0)).setTextSize(20);
        Toast.makeText(this, textView.getText()+" Selected", Toast.LENGTH_SHORT).show();
        Util util = new Util();
        util.fillList(List, ComboBox, com.rastrahm.app.listtask.model.GlobalGroups.getGlobal());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void getAction() {
        Refresh();
    }

    public void Refresh() {
        Util util = new Util();
        String value = ComboBox.getItemAtPosition(ComboBox.getSelectedItemPosition()).toString();
        util.fillSpinner(ComboBox, com.rastrahm.app.listtask.model.GlobalGroups.getGlobal());
        if (!com.rastrahm.app.listtask.model.GlobalGroups.isErraseGroup()) {
            ComboBox.setSelection(selCombo);
        }
        util.fillList(List, ComboBox, com.rastrahm.app.listtask.model.GlobalGroups.getGlobal());
    }

    @Override
    public void searchTasks(String[] search) {
        Util util = new Util();
        ArrayAdapter aa = (ArrayAdapter) ComboBox.getAdapter();
        int pos = -1;
        for (int i = 0; i < aa.getCount(); i++) {
            if (aa.getItem(i).toString().equals(search[0])) {
                pos = i;
                break;
            }
        }
        if (pos > -1) {
            ComboBox.setSelection(pos);
            util.fillList(List, ComboBox, com.rastrahm.app.listtask.model.GlobalGroups.getGlobal());
            if (!search[1].equals("")) {
                pos = -1;
                aa = (ArrayAdapter) List.getAdapter();
                for (int i = 0; i < aa.getCount(); i++) {
                    if (aa.getItem(i).toString().equals(search[1])) {
                        pos = i;
                        break;
                    }
                }
                if (pos > -1) {
                    //TODO: Marcar la selección, el metodo child retorna null
                    aa.notifyDataSetChanged();
                    List.setSelection(pos);
                    List.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    List.setItemChecked(pos, true);
                }
            }
        }
    }
}