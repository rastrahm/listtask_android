/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rastrahm.app.listtask.model;

/**
 * Super clase (Clase Global) de los grupos, permitiendo cargarlo y mantenerlo hasta el cierre de la aplicación
 * @author Rolando Strahm
 */
public class GlobalGroups {

    public static Groups Global;
    private static boolean ErraseGroup;
    
    /**
     * Constructor del GlobalGroup
     * @param group : Groups que se colocan en forma global
     * @see com.rastrahm.app.listtask.model.Groups
     */
    public GlobalGroups(Groups group) {
        ErraseGroup = false;
        Global = group;
    }
    
    /**
     * Coloca el grupo de forma global
     * @param group : Groups a colocar
     * @see com.rastrahm.app.listtask.model.Groups
     */
    public static void setGlobal(Groups group) {
        Global = group;
    }
    
    /**
     * Retorna el grupo guardado globalmente
     * @return Groups : Grupos guardados
     */
    public static Groups getGlobal() {
        return Global;
    }

    public static boolean isErraseGroup() {
        return ErraseGroup;
    }

    public static void setErraseGroup(boolean erraseGroup) {
        ErraseGroup = erraseGroup;
    }
}
