package com.rastrahm.app.listtask.controler;

import android.graphics.Color;
import android.os.Environment;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.rastrahm.app.listtask.model.Groups;
import com.rastrahm.app.listtask.model.TaskGroup;
import com.rastrahm.app.listtask.model.Tasks;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Clase que contiene las utilidades
 * @author Rolando Strahm
 */

public class Util {

    //public static String result;

    Config Conf = new Config("res/raw/listtask.properties");
    
    /**
     * Busca un String "search" en un array de String "values" y retorna la posición en caso de encontrarlo, utiliza indexOf asi que se comporta
     * como "que contiene a"
     * @param search : String a buscar
     * @param values : Array de String donde se buscara
     * @return int : -1: no encontro la cadena; 0: o mayor, 1ra posición del String dentro del array
     */
    public static int arraySearchPos(String search, String[] values) {
        int result = -1;
        for (int i = 0; i < values.length; i++) {
            if (values[i].indexOf(search) > 0) {
                result = i;
                break;
            }
        }
        return result;
    }

    /**
     * Carga el json que contiene el objeto Groups, según el caso de Archivo (propiedad file), 
     * o a través de la Web (propiedad url) según la propiedad method
     * @return Groups : Grupos contenidos en el json
     */
    public Groups loadGroup() {
	    Json jsonUtils = new Json();
        String json = null;
        if (Conf.getProperty("method").equals("file")) {
            FileUtils Read = new FileUtils();
            String file = Conf.getProperty("file");
            File sdcard = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
            File content = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),file);
            if (!content.exists()) {
                try {
                    content.createNewFile();
                    String value = "{\"group\":[{\"id\":1,\"name\":\"Grupo 1\",\"task\":[{\"id\":1,\"name\":\"Tarea 1.1\"}]}]}";
                    com.rastrahm.app.listtask.controler.FileUtils.Writer(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)), file, value);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            json = Read.Read( Environment.getExternalStorageDirectory() + "/Documents/" + file);
            ///data/user/0/com.rastrahm.app.listtask/files
            ///storage/emulated/0/Documents/listtask.json
        } else {
            URL url = null;
            try {
                url = new URL(Conf.getProperty("url"));
                //new HTTP().execute(url);
                String result = new HTTP().execute(url).get();
                if (result.length() > 0) {
                    json = result.substring(1, result.length());
                    json = json.substring(0, json.length() - 1);
                } else {
                    json =  "{\"group\":[{\"id\":1,\"name\":\"Grupo 1\",\"task\":[{\"id\":1,\"name\":\"Tarea 1.1\"}]}]}";
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

        }
        Groups tasks = jsonUtils.fromJson(json);
        com.rastrahm.app.listtask.model.GlobalGroups.setGlobal(tasks);
        return tasks;
    }
    
    public void fillSpinner(Spinner combo, Groups groups) {
        List <TaskGroup> list = groups.getGroup();
        String[] values = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            values[i] = list.get(i).getName();
        }
        ArrayAdapter aa = new ArrayAdapter(combo.getContext(), android.R.layout.simple_spinner_item, values);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        combo.setAdapter(aa);
    }


    public void fillList(ListView list, Spinner combo, Groups tasks) {
        String search = combo.getItemAtPosition(combo.getSelectedItemPosition()).toString();
        List<Tasks> lists = searchTasks(search, tasks);
        String[] values = new String[lists.size()];
        int i = 0;
        for (Tasks value : lists) {
            values[i] = value.getName();
            i++;
        }
        ArrayAdapter aa = new ArrayAdapter(list.getContext(), android.R.layout.simple_spinner_item, values) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView item = (TextView) super.getView(position, convertView, parent);
                item.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
                item.setHintTextColor(Color.LTGRAY);
                item.setLinkTextColor(Color.BLUE);
                item.setPadding(5, 5, 5, 5);
                item.setSelected(true);

                return item;
            }
        };
        list.setAdapter(aa);
    }

    /**
     * Busca una tarea
     * @param search : String cadena a buscar
     * @param tasks : Grupo donde se busca
     * @return List Task : Lista de tareas resultante
     */
    public List<Tasks> searchTasks(String search, Groups tasks) {
        List <TaskGroup> lists = tasks.getGroup();
        for (int i = 0; i < lists.size(); i++) {
            if (lists.get(i).getName().equals(search)) {
                List <Tasks> list = lists.get(i).getTasks();
                return list;
            }
        }
        return null;
    }
    
    /**
     * Busca un grupo dentro del objeto global GlobalGroup
     * @param search : String con la cadena a buscar
     * @return int : Número del grupo buscado
     */
    public int searchGroup(String search) {
        int pos = -1;
        //Recorrer el Global para buscar el TaskGroup
        for (TaskGroup task : com.rastrahm.app.listtask.model.GlobalGroups.getGlobal().getGroup()) {
            if ((task.getName() != null) && (task.getName().equals(search))) {
                pos = task.getId();
                break;
            }
        }
        return pos;
    }
    
    /**
     * Busca un string dentro del objeto Groups
     * @param search : String a buscar
     * @param task : Groups donde se busca
     * @return String [] : Array que contiene el nombre del grupo y el valor de la tarea
     */
    public String[] searchString(String search, Groups task) {
        String [] response = new String[2];
        for (TaskGroup group : task.getGroup()) {
            if (group.getName().indexOf(search) > 0) {
                response[0] = group.getName();
                response[1] = "";
                break;
            } else {
                for (Tasks tasks : group.getTasks()) {
                    if (tasks.getName().indexOf(search) > 0) {
                        response[0] = group.getName();
                        response[1] = tasks.getName();
                        break;
                    }
                }
            }
        }
        return response;
    }
    
    /**
     * Devuelve el contenido seleccionado del ComboBox
     * @param combo : JComboBox 
     * @return String : Valor seleccionado
     */
    /*public String getGroup(JComboBox combo) {
        return combo.getSelectedItem().toString();
    }*/
    
    /**
     * Retorna el valor seleccionado dentro del JList
     * @param list : JList
     * @return String : Valor seleccionado dentro del JList
     */
    /*public String getTask(JList list) {
        return list.getSelectedValue().toString();
    }*/
    
    /**
     * Guarda el cotendio de la clase global GroupsGlobal, en el archivo (siempre) y la web (popiedad activa)
     * @return boolean : True guardo en el servidor, False error en alguno de los parametros
     */
    public boolean saveJson() throws MalformedURLException, ExecutionException, InterruptedException {
        Json jsonUtils = new Json();
        if (!Conf.getProperty("method").equals("file")) {
            URL url = new URL("https://www.google.com");
            //new HTTP().execute(url);
            String result = new HTTP().execute(url).get();
        }
        String file = Conf.getProperty("file");
        if (com.rastrahm.app.listtask.controler.FileUtils.Writer(String.valueOf(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)), file, jsonUtils.toJson(com.rastrahm.app.listtask.model.GlobalGroups.getGlobal()))) {
            return true;
        }
        return false;
    }
    
    /**
     * Retorna una propiedad de forma universal 
     * @param prop : String con la clave de la propiedad
     * @return String con el valor de la propiedad
     */
    public String getData(String prop) {
	    return Conf.getProperty(prop);
    }
    
    /**
     * Coloca un valor dentro de la propiedad
     * @param key : String con el clave a guardar
     * @param value : String con el valor de la propiedad
     */
    public void setData(String key, String value) {
	    Conf.setProperty(key, value);
    }
    
    /**
     * Guarda las propiedades en el archivo de configuración
     */
    public void saveData() {
        Conf.saveProperty("src/main/java/com/rastrahm/app/listtask/resource/listtask.properties");
    }
}
